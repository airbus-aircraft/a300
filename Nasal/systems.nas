# AIRBUS A300 SYSTEMS FILE
##########################

## LIVERY SELECT
################

aircraft.livery.init("Aircraft/A300/Models/Liveries/" ~ getprop("sim/aero"));

setlistener("sim/model/livery/texture", func
 {
 var base = getprop("sim/model/livery/texture");
 setprop("sim/model/livery/texture-path[0]", "../Models/" ~ base);
 setprop("sim/model/livery/texture-path[1]", "../../Models/" ~ base);
 }, 1, 1);

## LIGHTS
#########

# create all lights
var beacon_switch = props.globals.getNode("controls/switches/beacon", 2);
var beacon = aircraft.light.new("sim/model/lights/beacon", [0.015, 3], "controls/lighting/beacon");

var strobe_switch = props.globals.getNode("controls/switches/strobe", 2);
var strobe = aircraft.light.new("sim/model/lights/strobe", [0.025, 1.5], "controls/lighting/strobe");

## ENGINES
##########

# trigger engine failure when the "on-fire" property is set to true
var failEngine = func(engineNo)
 {
 props.globals.getNode("sim/failure-manager/engines/engine[" ~ engineNo ~ "]/serviceable").setBoolValue(0);
 };
setlistener("engines/engine[0]/on-fire", func
 {
 if (props.globals.getNode("engines/engine[0]/on-fire").getBoolValue())
  {
  failEngine(0);
  }
 }, 0, 0);
setlistener("engines/engine[1]/on-fire", func
 {
 if (props.globals.getNode("engines/engine[1]/on-fire").getBoolValue())
  {
  failEngine(1);
  }
 }, 0, 0);

# startup/shutdown functions
var startup = func
 {
 setprop("controls/engines/engine[0]/cutoff", 1);
 setprop("controls/engines/engine[1]/cutoff", 1);
 setprop("controls/engines/engine[0]/starter", 1);
 setprop("controls/engines/engine[1]/starter", 1);
 setprop("controls/electric/avionics-switch", 1);
 setprop("controls/electric/battery-switch", 1);
 setprop("controls/lighting/instruments-norm", 0.8);

 settimer(func
  {
  setprop("controls/engines/engine[0]/cutoff", 0);
  setprop("controls/engines/engine[1]/cutoff", 0);
  }, 2);
 };
var shutdown = func
 {
 setprop("controls/engines/engine[0]/cutoff", 1);
 setprop("controls/engines/engine[1]/cutoff", 1);
 setprop("controls/electric/avionics-switch", 0);
 setprop("controls/electric/battery-switch", 0);
 setprop("controls/lighting/instruments-norm", 0);
 };

# listener to activate these functions accordingly
setlistener("sim/model/start-idling", func(idle)
 {
 var run = idle.getBoolValue();
 if (run)
  {
  startup();
  }
 else
  {
  shutdown();
  }
 }, 0, 0);

## GEAR
#######

# prevent retraction of the landing gear when any of the wheels are compressed
setlistener("controls/gear/gear-down", func
 {
 var down = props.globals.getNode("controls/gear/gear-down").getBoolValue();
 if (!down and (getprop("gear/gear[0]/wow") or getprop("gear/gear[1]/wow") or getprop("gear/gear[2]/wow")))
  {
  props.globals.getNode("controls/gear/gear-down").setBoolValue(1);
  }
 });

## DOORS
########

# create all doors
# front doors
var doorl1 = aircraft.door.new("sim/model/door-positions/doorl1", 2);
var doorr1 = aircraft.door.new("sim/model/door-positions/doorr1", 2);

# side doors
var doorl2 = aircraft.door.new("sim/model/door-positions/doorl2", 2);
var doorr2 = aircraft.door.new("sim/model/door-positions/doorr2", 2);

# rear doors
var doorl4 = aircraft.door.new("sim/model/door-positions/doorl4", 2);
var doorr4 = aircraft.door.new("sim/model/door-positions/doorr4", 2);

# cargo holds
var cargobulk = aircraft.door.new("sim/model/door-positions/cargobulk", 2.5);
var cargoaft = aircraft.door.new("sim/model/door-positions/cargoaft", 2.5);
var cargofwd = aircraft.door.new("sim/model/door-positions/cargofwd", 2.5);

# door opener/closer
var triggerDoor = func(door, doorName, doorDesc)
 {
 if (getprop("sim/model/door-positions/" ~ doorName ~ "/position-norm") > 0)
  {
  gui.popupTip("Closing " ~ doorDesc ~ " door");
  door.toggle();
  }
 else
  {
  if (getprop("velocities/groundspeed-kt") > 5)
   {
   gui.popupTip("You cannot open the doors while the aircraft is moving");
   }
  else
   {
   gui.popupTip("Opening " ~ doorDesc ~ " door");
   door.toggle();
   }
  }
 };
